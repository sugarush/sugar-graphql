import json
import asyncio
from datetime import datetime

from quart import Quart

from sugar_asynctest import AsyncTestCase

from sugar_graphql import QuartSubscriptionServer

from graphql import graphql
from graphene import ObjectType, String, Schema, Field


class Query(ObjectType):
    hello = String()

    def resolve_hello(root, info):
        return 'Hello, world!'


class Subscription(ObjectType):
    time_of_day = Field(String)

    async def subscribe_time_of_day(root, info):
        while True:
            yield { 'time_of_day': datetime.now().isoformat()}
            await asyncio.sleep(1)


schema = Schema(query=Query, subscription=Subscription)


class GraphQLViewTest(AsyncTestCase):

    default_loop = True

    async def asyncSetUp(self):
        subscription_server = QuartSubscriptionServer(schema)
        self.app = Quart('test')
        @self.app.websocket('/graphql')
        async def graphql_websocket():
            await subscription_server.handle()
        self.client = self.app.test_client()

    async def test_subscription(self):
        async with self.client.websocket('/graphql') as socket:
            await socket.send({ 'type': 'start', 'payload': { 'query': 'subscription { timeOfDay }' } })
            print(await socket.receive())
