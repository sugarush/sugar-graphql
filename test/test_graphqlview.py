import json

from quart import Quart

from sugar_asynctest import AsyncTestCase

from sugar_graphql import GraphQLView

from graphql import graphql
from graphene import ObjectType, String, Schema

class Query(ObjectType):
    hello = String(name=String(default_value="stranger"))

    async def resolve_hello(root, info, name):
        return f'Hello {name}!'

schema = Schema(query=Query)

class GraphQLViewTest(AsyncTestCase):

    default_loop = True

    async def asyncSetUp(self):
        self.app = Quart('test')
        self.app.add_url_rule('/graphql', view_func=GraphQLView.as_view('graphql', schema=schema, batch=True))
        self.client = self.app.test_client()

    async def test_json(self):
        response = await self.client.post('/graphql', data=json.dumps({ 'query': '{ hello }' }), headers={ 'Content-Type': 'application/json' })
        data = json.loads(await response.get_data())
        self.assertEqual(data['data']['hello'], 'Hello stranger!')

    async def test_graphql(self):
        response = await self.client.post('/graphql', data='{ hello }', headers={ 'Content-Type': 'application/graphql' })
        data = json.loads(await response.get_data())
        self.assertEqual(data['data']['hello'], 'Hello stranger!')

    async def test_batch(self):
        response = await self.client.post('/graphql', data=json.dumps([{ 'query': '{ hello }' }, { 'query': '{ hello }' }]), headers={ 'Content-Type': 'application/json' })
        #data = json.loads(await response.get_data())
        print(await response.get_data())
        #self.assertEqual(data[1]['data']['hello'], 'Hello stranger!')
