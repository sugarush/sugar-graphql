import asyncio
from inspect import isawaitable

from quart import websocket

#from graphql.execution.executors.asyncio import AsyncioExecutor

from graphql_ws import BaseConnectionContext, BaseSubscriptionServer
from graphql_ws.base import ConnectionClosedException
from graphql_ws.constants import GQL_CONNECTION_ACK, GQL_CONNECTION_ERROR, GQL_COMPLETE
from graphql_ws.observable_aiter import setup_observable_extension


setup_observable_extension()


class QuartConnectionContext(BaseConnectionContext):

    async def send(self, data):
        await self.ws.send(data)

    async def receive(self):
        return await self.ws.receive()

class QuartSubscriptionServer(BaseSubscriptionServer):

    def get_graphql_params(self, *args, **kwargs):
        params = super(QuartSubscriptionServer,
                       self).get_graphql_params(*args, **kwargs)
        #return dict(params, return_promise=True, executor=AsyncioExecutor())
        return dict(params, return_promise=True)

    async def handle(self):
        connection_context = QuartConnectionContext(websocket)
        await self.on_open(connection_context)
        try:
            while True:
                message = await connection_context.receive()
                task = asyncio.create_task(self.on_message(connection_context, message))
        except asyncio.CancelledError as e:
            await self.on_close(connection_context)
            raise e

    async def on_open(self, connection_context):
        pass

    async def on_close(self, connection_context):
        pass

    async def on_connection_init(self, connection_context, op_id, payload):
        try:
            await self.on_connect(connection_context, payload)
            await self.send_message(connection_context, op_type=GQL_CONNECTION_ACK)
        except Exception as e:
            await self.send_error(connection_context, op_id, e, GQL_CONNECTION_ERROR)
            await connection_context.close(1011)

    async def on_start(self, connection_context, op_id, params):
        execution_result = self.execute(
            connection_context.request_context, params)

        if isawaitable(execution_result):
            execution_result = await execution_result

        if not hasattr(execution_result, '__aiter__'):
            await self.send_execution_result(
                connection_context, op_id, execution_result)
        else:
            iterator = await execution_result.__aiter__()
            connection_context.register_operation(op_id, iterator)
            async for single_result in iterator:
                if not connection_context.has_operation(op_id):
                    break
                await self.send_execution_result(connection_context, op_id, single_result)
            await self.send_message(connection_context, op_id, GQL_COMPLETE)

    async def on_stop(self, connection_context, op_id):
        self.unsubscribe(connection_context, op_id)
