import asyncio
from functools import partial

from graphene.types.schema import Schema
#from graphql.execution.executors.asyncio import AsyncioExecutor

#from graphql_server import (HttpQueryError, default_format_error,
from graphql_server import (HttpQueryError, #default_format_error,
                            encode_execution_results, json_encode,
                            load_json_body, run_http_query)

from quart.views import View, request


class GraphQLView(View):

    methods = ['GET', 'POST']

    encode = staticmethod(json_encode)
    #format_error = staticmethod(default_format_error)

    def __init__(self, schema=None, batch=False):
        super(GraphQLView, self).__init__()
        print(type(schema))
        if not issubclass(type(schema), Schema):
            raise Exception('GraphQLView requires a schema.')
        self.schema = schema
        self.batch = batch

    async def dispatch_request(self):
        data = None
        content_type = request.headers.get('Content-Type')
        if content_type == 'application/graphql':
            data = {'query': (await request.get_data()).decode('utf8')}
        elif content_type == 'application/json':
            data = load_json_body((await request.get_data()).decode('utf8'))
        else:
            data = { }
        try:
            execution_results, all_params = run_http_query(
                self.schema,
                request.method.lower(),
                data,
                query_data=request.args,
                batch_enabled=self.batch,
                run_sync=False,
                #executor=AsyncioExecutor(),
                return_promise=True
            )
            execution_results = await asyncio.gather(*execution_results)
            result, status_code = encode_execution_results(
                execution_results,
                is_batch=isinstance(data, list),
                encode=self.encode,
                #format_error=self.format_error
            )
            return result, status_code
        except HttpQueryError as e:
            return self.encode({ 'errors': [self.format_error(e) ] }), e.status_code, e.headers
